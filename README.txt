1. Get code from: https://bitbucket.org/ACunningham32/supervised-learning/src/master/

2. Clone the Repo

3. run 'pip install requirements.txt'

4. To run different algorithms:

    python classify.py [baseball OR income] [ALGORITHM] [NUM_SAMPLES]

ie. To run the baseball pitch classification with Decision Tree and 100 samples

    python classify.py baseball 0 100

ie. To run the income classification with Neural Network and 100 samples

    python classify.py income 1 100

Algorithms:
0 - Decision Tree
1 - Neural Network
2 - Boosting
3 - SVM Linear SVC
4 - SVM Polynomial SVC
5 - k-Nearest Neighbors

**NOTE**
Max samples for basebal is 100,000
Max samples for income is 32,500

