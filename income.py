import numpy as np
import pandas
import time
from sklearn import svm
from sklearn import preprocessing


input_file = "data/income.csv"
# max_rows is 32561

def get_data(n):

    # comma delimited is the default
    data = pandas.read_csv(input_file, header = 0, nrows=int(n))

    # cleanse data
    clean_data = data[~data.isin([np.nan, np.inf, -np.inf]).any(1)]
    data = clean_data

    # encode the feature classes
    encoding_start = time.time()

    # workclass
    workclass = ['Private', 'Self-emp-not-inc', 'Self-emp-inc', 'Federal-gov', 'Local-gov', 'State-gov', 'Without-pay', 'Never-worked', 'Unknown']
    workclass_encoder = preprocessing.LabelEncoder()
    workclass_encoder.fit(workclass)
    workclass_encoded = workclass_encoder.transform(list(data['workclass']))
    data['workclass'] = pandas.Series(workclass_encoded, index=data.index)
    # print(data['workclass'])

    # education
    education = ['Bachelors', 'Some-college', '11th', 'HS-grad', 'Prof-school', 'Assoc-acdm', 'Assoc-voc', '9th', '7th-8th', '12th', 'Masters', '1st-4th', '10th', 'Doctorate', '5th-6th', 'Preschool', 'Unknown']
    education_encoder = preprocessing.LabelEncoder()
    education_encoder.fit(education)
    education_encoded = education_encoder.transform(list(data['education']))
    data['education'] = pandas.Series(education_encoded, index=data.index)
    # print(data['education'])

    # occupation
    occupation = ['Tech-support', 'Craft-repair', 'Other-service', 'Sales', 'Exec-managerial', 'Prof-specialty', 'Handlers-cleaners', 'Machine-op-inspct', 'Adm-clerical', 'Farming-fishing', 'Transport-moving', 'Priv-house-serv', 'Protective-serv', 'Armed-Forces', 'Unknown']
    occupation_encoder = preprocessing.LabelEncoder()
    occupation_encoder.fit(occupation)
    occupation_encoded = occupation_encoder.transform(list(data['occupation']))
    data['occupation'] = pandas.Series(occupation_encoded, index=data.index)
    # print(data['occupation'])

    # marital status
    marital_status = ['Married-civ-spouse', 'Divorced', 'Never-married', 'Separated', 'Widowed', 'Married-spouse-absent', 'Married-AF-spouse', 'Unknown']
    marital_status_encoder = preprocessing.LabelEncoder()
    marital_status_encoder.fit(marital_status)
    marital_status_encoded = marital_status_encoder.transform(list(data['marital.status']))
    data['marital.status'] = pandas.Series(marital_status_encoded, index=data.index)
    # print(data['marital.status'])

    # relationship
    relationship = ['Wife', 'Own-child', 'Husband', 'Not-in-family', 'Other-relative', 'Unmarried', 'Unknown']
    relationship_encoder = preprocessing.LabelEncoder()
    relationship_encoder.fit(relationship)
    relationship_encoded = relationship_encoder.transform(list(data['relationship']))
    data['relationship'] = pandas.Series(relationship_encoded, index=data.index)
    # print(data['relationship'])

    # race
    race = ['White', 'Asian-Pac-Islander', 'Amer-Indian-Eskimo', 'Other', 'Black', 'Unknown']
    race_encoder = preprocessing.LabelEncoder()
    race_encoder.fit(race)
    race_encoded = race_encoder.transform(list(data['race']))
    data['race'] = pandas.Series(race_encoded, index=data.index)
    # print(data['race'])

    # sex
    sex = ['Female', 'Male', 'Unknown']
    sex_encoder = preprocessing.LabelEncoder()
    sex_encoder.fit(sex)
    sex_encoded = sex_encoder.transform(list(data['sex']))
    data['sex'] = pandas.Series(sex_encoded, index=data.index)
    # print(data['sex'])

    # native country
    native_country = ['United-States', 'Cambodia', 'England', 'Puerto-Rico', 'Canada', 'Germany', 'Outlying-US(Guam-USVI-etc)', 'India', 'Japan', 
                        'Greece', 'South', 'China', 'Cuba', 'Iran', 'Honduras', 'Philippines', 'Italy', 'Poland', 'Jamaica', 'Vietnam', 'Mexico', 'Portugal', 
                        'Ireland', 'France', 'Dominican-Republic', 'Laos', 'Ecuador', 'Taiwan', 'Haiti', 'Columbia', 'Hungary', 'Guatemala', 'Nicaragua', 
                        'Scotland', 'Thailand', 'Yugoslavia', 'El-Salvador', 'Trinadad&Tobago', 'Peru', 'Hong', 'Holand-Netherlands', 'Unknown']
    native_country_encoder = preprocessing.LabelEncoder()
    native_country_encoder.fit(native_country)
    native_country_encoded = native_country_encoder.transform(list(data['native.country']))
    data['native.country'] = pandas.Series(native_country_encoded, index=data.index)
    # print(data['native.country'])

    # income
    income = ['<=50K', '>50K']
    target_classes = list(data['income'])
    target_names = income

    encoding_end = time.time()

    encoding_time = encoding_end - encoding_start
    # print(encoding_time)

    data = data.drop(columns=['income'])
    feature_names = list(data.columns.values)
    return data, target_classes, feature_names, target_names

