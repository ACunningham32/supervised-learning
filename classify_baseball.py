import sys
import decision_tree_classifier
import neural_network_classifier

def main():

    num_samples = 100
    k = 0

    if len(sys.argv) > 3:
        k = sys.argv[3]

    if len(sys.argv) > 2:
        num_samples = sys.argv[2]

    if len(sys.argv) > 1:
        if 0 == int(sys.argv[1]):
            decision_tree_classifier.run(num_samples)

        elif 1 == int(sys.argv[1]):
            neural_network_classifier.run(num_samples)


if __name__ == "__main__":
    main()