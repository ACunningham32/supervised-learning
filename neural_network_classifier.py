import baseball
import time
from sklearn.model_selection import train_test_split as tts
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier

def run(n):

    num_samples = n
    
    pitch_data, target_classes, feature_names, target_names = baseball.get_data(num_samples)
    train_feats, test_feats, train_labels, test_labels = tts(pitch_data, target_classes, test_size=0.2)

    # # Decision Tree Classifier
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5)

    # # print the details of the Classifier used
    print("Using", clf)

    # # training
    training_time_start = time.time()
    clf.fit(train_feats, train_labels)
    training_time_end = time.time()

    training_time = training_time_end - training_time_start

    # # predictions
    testing_time_start = time.time()
    predictions = clf.predict(test_feats)
    testing_time_end = time.time()

    testing_time = testing_time_end - testing_time_start

    # # or, just do this for accuracy
    print("\nAccuracy: {}".format(round(accuracy_score(test_labels, predictions), 4) * 100))
    print("Training time: {}".format(round(training_time, 4)))
    print("Testing time: {}".format(round(testing_time, 4)))
