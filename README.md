
# Getting Started
Run this command to install requirements:
```pip install requirements.txt```

Run the classify baseball file with arguments:
```python classify.py baseball [ALGORITHM] [NUM_SAMPLES]```

**Algorithm Numbers**
0 - Decision Tree Classifier
1 - Neural Network
2 - Boosted Decision Tree
3 - SVM 1st kernel
4 - SVM 2nd kernel
5 - kNN


Example:
```python classify_baseball.py 0 1000```
Will run the Decision Tree Classifier with 1,000 samples

```python classify_baseball.py 5 1000 5```
Will run kNN with 1,000 samples and k=5

# Classifying Baseball Pitches
Given the following features, how well can the algorithms classify the type of pitch thrown?

Data source: https://www.kaggle.com/pschale/mlb-pitch-data-20152018#pitches.csv

## Pitch Type Definitions:

CH - Changeup
CU - Curveball
EP - Eephus*
FC - Cutter
FF - Four-seam Fastball
FS - Splitter
FT - Two-seam Fastball
IN - Intentional ball
KC - Knuckle curve
KN - Knuckeball
PO - Pitchout
SC - Screwball*
SI - Sinker
SL - Slider
UN - Unknown*

*these pitch types occur rarely

## Features Definitions

ax -
ay -
az -
b_count - balls in the current count
b_score - score for the batter's team
break_angle
break_length
break_y
end_speed - speed of the pitch when it reaches the plate (commonly known as the pitch velocity)
nasty - percentage of how "nasty" the pitch is, unknown how this derived
outs - number of outs before the pitch is thrown
pfx_x
pfx_z
pitch_num - pitch number of at-bat
px - x-location as pitch crosses the plate. x=0 means right down the middle
pz - z-location as pitch crosses the plate. z=0 means the ground
s_count - strikes in the current count
spin_dir - direction in which pitch is spinning, measured in degrees
spin_rate - the pitch's spin rate, measured in RPM
start_speed - speed of the pitch just as it's thrown
sz_bot - strike zone bottom
sz_top - strike zone top
type - strike, ball or in play
type_confidence - confidence in pitch_type classification
vx0
vy0
vz0
x
x0
y
y0
z0
zone

## Convert .dot file to .png
The Decision Tree Classifier exports a graph of the decision tree. Run the following command to convert the export graph into a png:
```dot -Tpng tree.dot -o tree.png```

## Algorithms
### Decision Trees
    - using form of pruning (max_depth, min leafs, min )
    - describe how split attributes (ie. information gain, GINI)
    DecisionTreeClassifier(criterion="entropy", splitter="best", max_depth=10)

### Neural Network

### Boosting (boosted version of Decision Tree)
    - also using form of pruning

### SVM
    - 2 different kernel functions

### kNN
    - use different values of k

## Analysis:
    - Describe classification problems: why interesting

    For each algorithm:
    - graph showing performance on both training and test data as a function of training size
        - training times / iterations
    - wall clock time
    - cross validation help
    - compare and contrast with others