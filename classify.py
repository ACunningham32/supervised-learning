import sys
import time
import baseball
import income
import numpy as np

from sklearn import svm
from sklearn import tree
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split as tts
from sklearn.model_selection import ShuffleSplit
from plot_learning_curve import plot_learning_curve
from plot_validation_curve import plot_validation_curve

def get_baseball_data(n, test_size=0.3):
    # max n is 100,000
    num_samples = n
    
    pitch_data, target_classes, feature_names, target_names = baseball.get_data(num_samples)
    return tts(pitch_data, target_classes, test_size=test_size), feature_names, target_names

def get_income_data(n, test_size=0.3):
    # max n is 32561
    num_samples = n

    income_data, target_classes, feature_names, target_names = income.get_data(num_samples)
    return tts(income_data, target_classes, test_size=test_size), feature_names, target_names

def run_classifier(clf, train_feats, test_feats, train_labels, test_labels, feature_names, target_names, DT=False, title=None):

    # # print the details of the Classifier used
    print("Using", clf)

    # # Training
    training_time_start = time.time()
    clf.fit(train_feats, train_labels)
    training_time_end = time.time()

    training_time = training_time_end - training_time_start

    # # Predictions
    testing_time_start = time.time()
    predictions = clf.predict(test_feats)
    testing_time_end = time.time()

    testing_time = testing_time_end - testing_time_start

    # # Performance
    print("\nAccuracy: {}".format(round(accuracy_score(test_labels, predictions), 4) * 100))
    print("Training time: {}".format(round(training_time, 4)))
    print("Testing time: {}".format(round(testing_time, 4)))

    # Only used for Decision Trees
    if DT:
        tree.export_graphviz(clf, out_file='tree.dot', feature_names=feature_names, class_names=target_names, special_characters=True)

    # print("-----------------Original Features--------------------")
    # print("Best score: %0.4f" % clf.best_score_)
    # print("Using the following parameters:")
    # print(clf.best_params_)

    # cv = ShuffleSplit(train_size=0.3, n_splits=10, random_state=0)
    # plot_time_start = time.time()
    # plot_learning_curve(clf, title, train_feats, train_labels, cv=cv).show()
    # # plot_validation_curve(clf, title, train_feats, train_labels, "n_neighbors", [1, 5, 10, 20, 30], cv=cv).show()
    # plot_time_end = time.time()
    # plotting_time = plot_time_end - plot_time_start
    # print("Plotting time: {}".format(round(plotting_time, 4)))
    # # plot_validation_curve(clf, title, train_feats, train_labels, "n_neighbors", [10, 15, 20, 30, 50], cv=cv).show()


def main():
    num_samples = 1000
    DT = False
    title = None

    if len(sys.argv) > 4:
        k = sys.argv[4]

    if len(sys.argv) > 3:
        num_samples = sys.argv[3]

    if len(sys.argv) > 2:
        if "baseball" == sys.argv[1]:
            train_and_test, feature_names, target_names = get_baseball_data(num_samples)
            train_feats, test_feats, train_labels, test_labels = train_and_test
        
        elif "income" == sys.argv[1]:
            train_and_test, feature_names, target_names = get_income_data(num_samples)
            train_feats, test_feats, train_labels, test_labels = train_and_test

        else:
            print("No dataset selected")
            return

        # Decision Tree
        if 0 == int(sys.argv[2]):
            # parameters = {'criterion': ['entropy', 'gini'], 'splitter': ['best', 'random'], 'max_depth': [None, 10, 15, 20], 'min_samples_split': [2, 3, 4],'min_impurity_decrease': [0., 0.01, 0.05], 'random_state': [0]}
            # {'splitter': 'best', 'min_impurity_decrease': 0.0, 'random_state': 0, 'criterion': 'entropy', 'min_samples_split': 3, 'max_depth': 10} income 85%
            # {'splitter': 'best', 'min_impurity_decrease': 0.0, 'random_state': 0, 'criterion': 'entropy', 'min_samples_split': 2, 'max_depth': 15} baseball 74%
            # clf = GridSearchCV(tree.DecisionTreeClassifier(), parameters, n_jobs=-1, verbose=True)
            clf = tree.DecisionTreeClassifier(criterion="entropy", splitter="best", max_depth=15, min_samples_split=2)
            title = "Decision Tree Learning Curve (Max Depth 15)"
            DT = True

        # Neural Network - a large amount of data changes the best functions to use
        elif 1 == int(sys.argv[2]):
            # parameters = {'solver': ['lbfgs', 'adam'], 'activation': ['identity', 'logistic', 'relu'], 'max_iter': [1500, 2000], 'alpha': [0.1], 'hidden_layer_sizes': [9], 'random_state':[0]}
            # {'solver': 'adam', 'activation': 'logistic', 'max_iter': 1500, 'random_state': 0, 'alpha': 0.1, 'hidden_layer_sizes': 9} baseball 68%
            # {'solver': 'adam', 'activation': 'relu', 'max_iter': 1500, 'random_state': 0, 'alpha': 0.1, 'hidden_layer_sizes': 9} income 78%
            # clf = GridSearchCV(MLPClassifier(), parameters, n_jobs=-1, verbose=True)
            clf = MLPClassifier(solver='adam', activation='logistic', max_iter=1500, random_state=0, alpha=0.1, hidden_layer_sizes=9)
            title = "Neural Network Learning Curve (Adam solver)"

        # Boosting
        elif 2 == int(sys.argv[2]):
            # parameters = {'learning_rate': [0.1, 0.2, 0.5, 0.7], 'n_estimators': [50, 100, 500], 'subsample': [1.0, 0.5, 0.25], 'max_depth': [5, 10, 15]}
            # {'n_estimators': 100, 'subsample': 0.5, 'learning_rate': 0.1, 'max_depth': 5} baseball 81% 75 seconds
            # {'n_estimators': 500, 'subsample': 1.0, 'learning_rate': 0.1, 'max_depth': 5} income 87 % 1 second
            # {'n_estimators': 50, 'subsample': 1.0, 'learning_rate': 0.2, 'max_depth': 5} income large data
            # clf = GridSearchCV(GradientBoostingClassifier(), parameters, n_jobs=-1, verbose=True)
            # clf = GradientBoostingClassifier(n_estimators=50, subsample=1.0, learning_rate=0.2, max_depth=5)
            clf = GradientBoostingClassifier(n_estimators=50, subsample=1.0, learning_rate=0.2, max_depth=5)
            title = "Boosting Learning Curve"

        # SVM 1 very slow
        elif 3 == int(sys.argv[2]):
            # parameters = {'C': [1, 10, 100], 'tol': [0.0001], 'max_iter': [1000, 1500, 2000]} #'gamma': [0.001, 0.0001], 'kernel': ['rbf', 'linear']}
            # clf = GridSearchCV(svm.LinearSVC(), parameters, n_jobs=-1, verbose=True)
            # {'C': 10, 'tol': 0.0001} income 78% 32,500 sometimes as low as 40%
            # {'kernel': 'linear', 'C': 1, 'gamma': 0.001} baseball 60%
            clf = svm.LinearSVC(C=10, tol=0.0001, max_iter=2000)
            # clf = svm.SVC(kernel='linear', C=1, gamma=0.001)
            title = "SVM Learning Curve (Linear kernel)"
            # title = "SVM Linear SVC Learning Curve"

        # SVM 2
        elif 4 == int(sys.argv[2]):
            # parameters = {'kernel': ['linear', 'rbf', 'poly', 'sigmoid'], 'C': [1, 10], 'gamma': [0.001, 0.01]}
            # {'kernel': 'poly', 'C': 1, 'gamma': 0.001} baseball
            # clf = GridSearchCV(svm.SVC(), parameters, n_jobs=-1, verbose=True)
            clf = svm.SVC(kernel='poly', gamma=0.001, C=1)
            title = "SVM Learning Curve (Polynomial kernel)"

        # kNN
        elif 5 == int(sys.argv[2]):
            # parameters = {'n_neighbors': [1, 3, 5, 7], 'weights': ['uniform', 'distance'], 'algorithm': ['ball_tree', 'kd_tree', 'brute']}
            # parameters = {'n_neighbors': [2, 5, 10, 20, 30], 'weights': ['uniform'], 'algorithm': ['ball_tree']} #baseball 60%
            # {'n_neighbors': 10, 'weights': 'uniform', 'algorithm': 'ball_tree'} #income 80%
            # clf = GridSearchCV(KNeighborsClassifier(), parameters, n_jobs=-1, verbose=True)
            clf = KNeighborsClassifier(weights='uniform', algorithm='ball_tree', n_neighbors=10)
            title = "kNN Learning Curve (k = 10)"
            # title = "kNN Scores With Different Values of k"

        run_classifier(clf, train_feats, test_feats, train_labels, test_labels, feature_names, target_names, DT=DT, title=title)


if __name__ == "__main__":
    main()